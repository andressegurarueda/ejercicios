package registro;

import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Registro {

    public static void main(String args[]) {

        String nombre = "", celular = "", correo = "", id = "", tipoId = "", apellido = "", password = "";
        boolean validacion;
        Scanner entrada = new Scanner(System.in);

        do {
            System.out.print("Ingresa tu nombre: ");
            nombre = entrada.nextLine();
            validacion = valCar(nombre);
            if (!validacion) {
                System.out.println("Error, Digita un nombre valido");
            }
        } while (!validacion);

        do {
            System.out.print("Ingresa tu apellido: ");
            apellido = entrada.nextLine();
            validacion = valCar(apellido);
            if (!validacion) {
                System.out.println("Error, Digita un apellido valido");
            }
        } while (!validacion);

        do {
            System.out.print("Ingresa tu celular: ");
            celular = entrada.nextLine();
            validacion = valNum(celular, 10);
            if (!validacion) {
                System.out.println("Error, Digita un celular valido");
            }
        } while (!validacion);

        do {
            validacion = false;
            System.out.print("Indica tu tipo de indentificacion (TI)(CC)(CE)(PA): ");
            tipoId = entrada.nextLine().toUpperCase();
            if (tipoId.equalsIgnoreCase("TI") || tipoId.equalsIgnoreCase("CC")
                    || tipoId.equalsIgnoreCase("CE") || tipoId.equalsIgnoreCase("PA")) {
                validacion = true;
            }
            if (!validacion) {
                System.out.println("Error, Digita un tipo de identificacion valido");
            }
        } while (!validacion);

        do {
            System.out.print("Ingresa tu identificacion: ");
            id = entrada.nextLine();
            switch (tipoId) {
                case "CE":
                case "TI":
                    validacion = valNum(id, 10);
                    break;
                case "CC":
                    validacion = valNum(id, 8);
                    break;
                case "PA":
                    validacion = valPas(id);
            }
            if (!validacion) {
                System.out.println("Error, Digita identificacion correcta");
            }
        } while (!validacion);

        do {
            System.out.print("Ingresa tu correo: ");
            correo = entrada.nextLine();
            validacion = valEmail(correo);
            if (!validacion) {
                System.out.println("Error, Digita un correo valido");
            }
        } while (!validacion);
    }

    public static boolean valCar(String nombre) {
        return Pattern.matches("[a-zA-Z]{3,10}", nombre);
    }

    public static boolean valNum(String val, int x) {
        return Pattern.matches("[0-9]{" + x + ",10}", val);
    }

    public static boolean valPas(String valpas) {
        return Pattern.matches("[a-zA-Z]{2,2}[0-9]{6,6}", valpas);
    }
    public static boolean valEmail(String valEmail) {
        return Pattern.matches("[a-zA-Z0-9]*@[a-zA-Z]+(\\.[a-zA-Z]{2,})", valEmail);
    }
}
