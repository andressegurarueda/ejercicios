package ordenarpalabras;

import java.util.ArrayList;
import java.util.Collections;


public class OrdenarPalabras {

    public static void main(String[] args) {
        
        ArrayList<String> arreglo = new ArrayList<String>();

        arreglo.add("Andres");
        arreglo.add("luisa");
        arreglo.add("paola");
        arreglo.add("Luis");
        arreglo.add("Jose");
        
        Collections.sort(arreglo);
        for (String item:arreglo) {
            System.out.println(item);
        }

    }

}
