package ordenar_num;

import java.util.Scanner;

public class Ordenar_num {

    public static void main(String[] args) {

        int tamaño = 0, aux = 0;
        Scanner entrada = new Scanner(System.in);
        System.out.print("Ingresa la cantidad de numeros a ordenar: ");
        tamaño = entrada.nextInt();

        int arreglo[] = new int[tamaño];
        for (int i = 0; i < arreglo.length; i++) {
            System.out.print("Ingresa el " + (i + 1) + " numero: ");
            arreglo[i] = entrada.nextInt();
        }
        
        for (int i = 0; i < arreglo.length - 1; i++) {
            for (int j = i; j < arreglo.length; j++) {
                if (arreglo[i] > arreglo[j]) {
                    aux = arreglo[i];
                    arreglo[i] = arreglo[j];
                    arreglo[j] = aux;
                }
            }

        }

        for (int i = 0; i < arreglo.length; i++) {
            System.out.print(arreglo[i] + " ");
        }

    }
}
