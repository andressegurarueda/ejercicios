package cantidadcaracter;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CantidadCaracter {

    public static void main(String[] args) {

        Scanner entrada = new Scanner(System.in);
        String palabra = "";
        Map<Character, Integer> map = new HashMap<Character, Integer>();
        char[] arreglo;
       
        System.out.print("Escriba una palabra: ");
        palabra = entrada.nextLine();
        arreglo = palabra.toCharArray();

        for (int j = 0; j < arreglo.length; j++) {
            if(map.get(arreglo[j]) != null ){
            map.put(arreglo[j], map.get(arreglo[j]) + 1);
            
            }else{
            map.put(arreglo[j],1);
            }
        }
        map.forEach((k,v) -> System.out.println("caracter: " + k + ": total: " + v));
        

    }

}
